import { LoggerStaticService } from '../core/service/logger-static.service';
import { Config, LoggerProvider } from '../core/type/loggerLevelTypes';
import { defaultConfig } from '../core/const/logger.const';

export function registerLogger(loggerProvider: LoggerProvider,
                               config: Config = defaultConfig): void {
  LoggerStaticService.register(loggerProvider, config);
}

export function changeLoggerConfig(config: Config) {
  LoggerStaticService.changeConfig(config);
}

export function debugClass(): ClassDecorator {
  return (target: Function): void => {
    Object.keys(target.prototype)
      .forEach((propertyName: string) => {
        const propertyDescriptor: PropertyDescriptor = Object.getOwnPropertyDescriptor(target.prototype, propertyName);

        if (propertyDescriptor.value instanceof Function) {
          const decoratedMethod: PropertyDescriptor = decorateMethod(propertyDescriptor, propertyName);

          Object.defineProperty(target.prototype, propertyName, decoratedMethod);
        }
      });
  };
}

function decorateMethod(originalMethodDescriptor: PropertyDescriptor, methodName: string): PropertyDescriptor {
  const originalMethod = originalMethodDescriptor.value;
  originalMethodDescriptor.value = function (...args: any[]) {
    LoggerStaticService.logDebug(`apdc-logger: enter to method '${methodName}' with arg: ${JSON.stringify(args)}`);

    let result;

    try {
      result = originalMethod.apply(this, args);

      if (result instanceof Promise) {
        result
          .catch((error) => {
            logError(error, methodName, true);

            return Promise.reject(error);
          });
      }
    } catch (error) {
      logError(error, methodName);

      throw(error);
    }

    LoggerStaticService.logDebug(`apdc-logger: exit method '${methodName}' with given value: ${JSON.stringify(result)}`);

    return result;
  };

  return originalMethodDescriptor;
}

function logError(error, methodName: string, isPromise: boolean = false) {
  const errorType = isPromise ? 'Promise' : 'Plain';

  if (error instanceof Error) {
    LoggerStaticService.logError(`apdc-logger: method '${methodName}' throw (${errorType}) error ${JSON.stringify(error.stack)}`);
  } else {
    LoggerStaticService.logError(`apdc-logger (${errorType}): method '${methodName}' throw (${errorType}) unknown error`);
  }
}
