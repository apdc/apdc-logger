import { Log, LoggerUtils } from '../util/logger.utils';
import { Config, LoggerProvider, LoggerLevelTypes } from '../type/loggerLevelTypes';
import { defaultConfig } from '../const/logger.const';

export class LoggerService {

  constructor(private loggerWriter: LoggerProvider,
              private config: Config = defaultConfig) {}

  public changeConfig(config: Config): void {
    this.config = config;
  }

  private log(data: string, type: LoggerLevelTypes): void {
    if (this.config.configLevelTypes.some(configLevelType => configLevelType === type)) {
      const log: Log = LoggerUtils.createLog(data, type);

      try {
        this.loggerWriter.onLog(log);
      } catch (error) {
        console.error('apdc-logger -> Unexpected error in delivered provider', error);
      }
    }
  }

  public logInfo(data: string): void {
    this.log(data, LoggerLevelTypes.INFO);
  }

  public logWarn(data: string): void {
    this.log(data, LoggerLevelTypes.WARN);
  }

  public logDebug(data: string): void {
    this.log(data, LoggerLevelTypes.DEBUG);
  }

  public logError(data: string): void {
    this.log(data, LoggerLevelTypes.ERROR);
  }

}
