import { Log, LoggerUtils } from '../util/logger.utils';
import { Config, LoggerProvider, LoggerLevelTypes } from '../type/loggerLevelTypes';
import { defaultConfig } from '../const/logger.const';

export class LoggerStaticService {

  private static loggerProvider: LoggerProvider;
  private static config: Config = defaultConfig;

  public static register(loggerProvider: LoggerProvider,
                         config: Config = defaultConfig) {
    LoggerStaticService.loggerProvider = loggerProvider;
    LoggerStaticService.config = config;
  }

  public static getMyself() {
    return LoggerStaticService;
  }

  public static changeConfig(config: Config) {
    LoggerStaticService.config = config;
  }

  private static log(data: string, type: LoggerLevelTypes): void {
    if (this.loggerProvider && LoggerStaticService.checkIfShouldBeLogged(type)) {
      const log: Log = LoggerUtils.createLog(data, type);

      try {
        LoggerStaticService.loggerProvider.onLog(log);
      } catch (error) {
        console.error('apdc-logger-static -> Unexpected error in delivered provider', error);
      }
    }
  }

  private static checkIfShouldBeLogged(type: LoggerLevelTypes): boolean {
    return LoggerStaticService.config.configLevelTypes
      .some(configLevelType => configLevelType === type);
  }

  public static logInfo(data: string): void {
    LoggerStaticService.log(data, LoggerLevelTypes.INFO);
  }

  public static logWarn(data: string): void {
    LoggerStaticService.log(data, LoggerLevelTypes.WARN);
  }

  public static logDebug(data: string): void {
    LoggerStaticService.log(data, LoggerLevelTypes.DEBUG);
  }

  public static logError(data: string): void {
    LoggerStaticService.log(data, LoggerLevelTypes.ERROR);
  }

}
