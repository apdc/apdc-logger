import { Config, LoggerLevelTypes } from '../type/loggerLevelTypes';

export const defaultConfig: Config = {
  configLevelTypes: [
    LoggerLevelTypes.INFO,
    LoggerLevelTypes.DEBUG,
    LoggerLevelTypes.WARN,
    LoggerLevelTypes.ERROR,
  ],
};
