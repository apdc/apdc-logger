import { LoggerLevelTypes } from '../type/loggerLevelTypes';

export interface Log {
  readonly timestamp: number;
  readonly type: LoggerLevelTypes;
  readonly data: string;
}

export class LoggerUtils {

  public static createLog(data: string, type: LoggerLevelTypes): Log {
    return {
      data,
      type,
      timestamp: Date.now(),
    };
  }

  public static defaultLogToString(log: Log) {
    return `[${log.type}][${log.timestamp}][${LoggerUtils.timestampToUTCString(log.timestamp)}]  ${log.data}`;
  }

  private static timestampToUTCString(timestamp: number) {
    return new Date(timestamp).toUTCString();
  }

}
