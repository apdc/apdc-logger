import { Log, LoggerUtils } from '../core/util/logger.utils';
import { AbstractLoggerProvider } from './abstract-logger.provider';

export class ConsoleLoggerProvider extends AbstractLoggerProvider {
  public static create() {
    return new ConsoleLoggerProvider();
  }

  private constructor() { // to make class final (should be not extendable)
    super();
  }

  protected onDebug(log: Log): void {
    console.debug(LoggerUtils.defaultLogToString(log));
  }

  protected onError(log: Log): void {
    console.error(LoggerUtils.defaultLogToString(log));
  }

  protected onInfo(log: Log): void {
    console.info(LoggerUtils.defaultLogToString(log));
  }

  protected onWarn(log: Log): void {
    console.warn(LoggerUtils.defaultLogToString(log));
  }

}
