import { EcLoggerProvider, LoggerProvider, LoggerProviderSettings } from '../../core/type/loggerLevelTypes';
import { ConsoleLoggerProvider } from '../console-logger.provider';
import { Ec365LoggerProvider } from '../ec365/ec365-logger.provider';

export class LoggerProviderFactory {

  public static createConsoleLoggerProvider(): LoggerProvider {
    return ConsoleLoggerProvider.create();
  }

  public static createEc365LoggerProvider(worksheetName: string, loggerSettings?: LoggerProviderSettings): EcLoggerProvider {
    return Ec365LoggerProvider.create(worksheetName, loggerSettings);
  }

}
