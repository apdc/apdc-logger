import { LoggerProvider, LoggerLevelTypes } from '../core/type/loggerLevelTypes';
import { Log } from '../core/util/logger.utils';

export abstract class AbstractLoggerProvider implements LoggerProvider {

  protected decisionMap = {
    [LoggerLevelTypes.INFO]: log => this.onInfo(log),
    [LoggerLevelTypes.DEBUG]: log => this.onDebug(log),
    [LoggerLevelTypes.ERROR]: log => this.onError(log),
    [LoggerLevelTypes.WARN]: log => this.onWarn(log),
  };

  public onLog(log: Log): void {
    this.decisionMap[log.type](log);
  }

  protected abstract onInfo(log: Log): void;
  protected abstract onDebug(log: Log): void;
  protected abstract onWarn(log: Log): void;
  protected abstract onError(log: Log): void;

}
