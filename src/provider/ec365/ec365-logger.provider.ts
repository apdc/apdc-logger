import { Log } from '../../core/util/logger.utils';
import { EcLoggerProvider, LoggerProviderSettings } from '../../core/type/loggerLevelTypes';
import { hiddenWorksheetServiceSingleton } from './excel/hidden-worksheet/hidden-worksheet.service';
import { excelWorksheetApiSingleton } from './excel/api/excel-worksheet.api';

interface LogsObject {
  [index: number]: Log;
}

const LOGGER_DEFAULT_SETTINGS: LoggerProviderSettings = {
  debounceTime: 2000,
  chunkCellSize: 500,
};

export class Ec365LoggerProvider implements EcLoggerProvider  {
  private queueLogsPromise: Promise<void> = Promise.resolve();

  private static ACTUAL_LOGS_KEY = 'ACTUAL_LOGS';
  private static PREVIOUSLY_LOGS_KEY = 'PREVIOUSLY_LOGS';

  private logsBuffer: Log[] = [];
  private areInitLogsFetched: boolean = false;

  private previouslyLogs: LogsObject = {};
  private bufferLogs: LogsObject = {};
  private debounceSave: boolean = true;

  public static create(worksheetName: string, loggerSettings: LoggerProviderSettings = LOGGER_DEFAULT_SETTINGS) {
    const settings = {
      ...LOGGER_DEFAULT_SETTINGS,
      ...loggerSettings,
    };

    return new Ec365LoggerProvider(worksheetName, settings);
  }

  private constructor(private worksheetName: string,
                      private loggerSettings: LoggerProviderSettings = {}) { // to make class final (should be not extendable)
    excelWorksheetApiSingleton.changeSettings({ chunkCellsSize: loggerSettings.chunkCellSize });
    this.initPreviouslyLogs();
  }

  public onLog(log: Log): void {
    if (this.areInitLogsFetched) {
      this.nextLog(log);
    } else {
      this.logsBuffer.push(log);
    }
  }

  public changeSettings(loggerSettings: LoggerProviderSettings) {
    const newLoggerSettings = {
      ...this.loggerSettings,
      ...loggerSettings,
    };

    if (this.validateSettings(newLoggerSettings)) {
      this.loggerSettings = newLoggerSettings;
      excelWorksheetApiSingleton.changeSettings({ chunkCellsSize: newLoggerSettings.chunkCellSize });
    } else {
      console.error('removeOlderThat has invalid value - changing settings will be ignored');
    }
  }

  private validateSettings(loggerSettings: LoggerProviderSettings): boolean {
    return Number.isInteger(loggerSettings.removeOlderThat) && loggerSettings.removeOlderThat > 0;
  }

  private nextLog(log: Log) {
    this.bufferLogs = this.createLogsObjectBasedOn(log, this.bufferLogs);
    if (this.debounceSave) {
      this.debounceSave = false;

      setTimeout(() => {
        this.saveLogsToWorksheet();
        this.debounceSave = true;
      },         this.loggerSettings.debounceTime);
    }
  }

  private saveLogsToWorksheet() {
    this.queueLogsPromise = this.queueLogsPromise
      .then(() => {
        return hiddenWorksheetServiceSingleton.setItem(this.worksheetName, Ec365LoggerProvider.ACTUAL_LOGS_KEY, this.bufferLogs)
          .then(() => hiddenWorksheetServiceSingleton.setItem(this.worksheetName, Ec365LoggerProvider.PREVIOUSLY_LOGS_KEY, this.previouslyLogs))
          .then(() => { this.previouslyLogs = this.bufferLogs; });
      })
      .catch((error) => {
        console.error('apdc-logger -> unexpected error during saving logs', error);
      });
  }

  private saveLogsFromBuffer() {
    this.logsBuffer.forEach(log => this.nextLog(log));
    this.logsBuffer = [];
  }

  private createLogsObjectBasedOn(log: Log, logs: LogsObject): LogsObject {
    const previouslyLogsWithRemovedToOldLogs = this.removeLogsOlderThat(logs);
    const nextIndex = this.generateNextIndex(previouslyLogsWithRemovedToOldLogs);

    return {
      ...previouslyLogsWithRemovedToOldLogs,
      [nextIndex]: log,
    };
  }

  private removeLogsOlderThat(logs: LogsObject): LogsObject {
    const logsToConvert: LogsObject = { ...logs };

    const pointToRemoveTimestamp = Date.now() - this.loggerSettings.removeOlderThat;

    Object.keys(logsToConvert).forEach((index: string) => {
      if (this.loggerSettings.removeOlderThat && logsToConvert[index].timestamp < pointToRemoveTimestamp) {
        delete logsToConvert[index];
      }
    });

    return logsToConvert;
  }

  private generateNextIndex(logsObject: LogsObject) {
    const indexes = Object.keys(logsObject);
    return indexes.length ? (1 + parseInt(indexes[indexes.length - 1], 10)) : 1; // everybody likes magic things ;)
  }

  private initPreviouslyLogs(): void {
    this.fetchActualLogs()
      .then((actualLogs: LogsObject) => {
        const fetchedLogs = actualLogs || {};
        this.previouslyLogs = fetchedLogs;
        this.bufferLogs = fetchedLogs;
      })
      .catch(() => {
        console.error('error - can not fetch initial logs');
      })
      .finally(() => {
        this.areInitLogsFetched = true;
        this.saveLogsFromBuffer();
      });
  }

  private fetchActualLogs(): Promise<LogsObject> {
    return hiddenWorksheetServiceSingleton.getItem(this.worksheetName, Ec365LoggerProvider.ACTUAL_LOGS_KEY);
  }

}
