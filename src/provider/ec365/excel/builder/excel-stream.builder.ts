import { FunctionLoaded, LoaderType } from '../api/loader-interface';

export class ExcelStreamBuilder<T> {
  private readonly collectedFunctions = [];

  private constructor(functions = []) {
    this.collectedFunctions = [...functions];
  }

  public static stream(): ExcelStreamBuilder<void> {
    return new ExcelStreamBuilder<void>();
  }

  public loadWithSync<S extends LoaderType<S>>(fun: FunctionLoaded<S>, propsToLoad?: string | string[]): ExcelStreamBuilder<S> {
    const nextFun = async (context: Excel.RequestContext): Promise<S> => {
      const valueToLoad: S = fun(context);
      valueToLoad.load(propsToLoad);
      await context.sync();

      return valueToLoad;
    };

    return new ExcelStreamBuilder<S>([...this.collectedFunctions, nextFun]);
  }

  public loadObjectWithSync<S extends LoaderType<S>>(fun: (prev: T) => S, propsToLoad?: string | string[]): ExcelStreamBuilder<S> {
    const nextFun = async (context: Excel.RequestContext, prev: T): Promise<S> => {
      const valueToLoad: S = fun(prev);
      valueToLoad.load(propsToLoad);
      await context.sync();
      return valueToLoad;
    };

    return new ExcelStreamBuilder<S>([...this.collectedFunctions, nextFun]);
  }

  public actionWithSync(fun: (context: Excel.RequestContext, prev?: T) => void): ExcelStreamBuilder<void> {
    const nextFun = async (context: Excel.RequestContext, prev: T): Promise<void> => {
      await fun(context, prev);
      await context.sync();
    };

    return new ExcelStreamBuilder<void>([...this.collectedFunctions, nextFun]);
  }

  public action(fun: (context: Excel.RequestContext, prev?: T) => void | Promise<void>): ExcelStreamBuilder<void> {
    const nextFun = async (context: Excel.RequestContext, prev: T): Promise<void> => {
      await fun(context, prev);
    };

    return new ExcelStreamBuilder<void>([...this.collectedFunctions, nextFun]);
  }

  public get<S>(fun: (context: Excel.RequestContext, prev?: T) => S): ExcelStreamBuilder<S> {
    const nextFun = async (context: Excel.RequestContext, prev: T): Promise<S> => {
      return fun(context, prev);
    };

    return new ExcelStreamBuilder<S>([...this.collectedFunctions, nextFun]);
  }

  public getWithSync<S>(fun: (context: Excel.RequestContext, prev?: T) => S): ExcelStreamBuilder<S> {
    const nextFun = async (context: Excel.RequestContext, prev: T): Promise<S> => {
      const returnedValue = fun(context, prev);
      await context.sync();
      return returnedValue;
    };

    return new ExcelStreamBuilder<S>([...this.collectedFunctions, nextFun]);
  }

  public map<S>(fun: (prev: T) => S): ExcelStreamBuilder<S> {
    const nextFun = (context: Excel.RequestContext, prev: T) => {
      return fun(prev);
    };

    return new ExcelStreamBuilder<S>([...this.collectedFunctions, nextFun]);
  }

  // TODO: error catching and identifier where is it
  public run(): Promise<T> {
    let prev: any;
    return Excel.run(async (context: Excel.RequestContext) => {
      for (const fun of this.collectedFunctions) {
        prev = await fun(context, prev);
      }

      return prev;
    });
  }

}
