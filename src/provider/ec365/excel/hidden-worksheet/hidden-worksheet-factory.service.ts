import { hiddenWorksheetWriterSingleton } from './hidden-worksheet-writer.service';
import { excelWorksheetApiSingleton } from '../api/excel-worksheet.api';

const SPECIAL_CHARS_TO_REMOVE = /[{}\-]/g;
const FIRST_ROW_INDEX = 0;
const WORKSHEET_NAME_MAX_LENGTH = 31;

export class HiddenWorksheetFactory {
  async getExistingOrCreate(worksheetId: string): Promise<string> {
    const nameOfHiddenWorksheet = this.getHiddenWorksheetName(worksheetId);

    const newWorksheetId = await excelWorksheetApiSingleton.fetchWorksheetIdByName(nameOfHiddenWorksheet);

    if (newWorksheetId) {
      return newWorksheetId;
    }

    return await this.createHiddenWorksheet(worksheetId);
  }

  public getHiddenWorksheetName(worksheetId: string): string {
    return worksheetId
      .replace(SPECIAL_CHARS_TO_REMOVE, '')
      .substr(0, WORKSHEET_NAME_MAX_LENGTH);
  }

  private async createHiddenWorksheet(worksheetId: string): Promise<string> {
    const nameOfHiddenWorksheet = this.getHiddenWorksheetName(worksheetId);
    const newWorksheetId = await excelWorksheetApiSingleton.createNewWorksheet(nameOfHiddenWorksheet, Excel.SheetVisibility.veryHidden);

    await hiddenWorksheetWriterSingleton
      .writeRow(newWorksheetId, FIRST_ROW_INDEX, ['$WORKSHEET_ID', 1, JSON.stringify(worksheetId)]);

    return newWorksheetId;
  }

}

export const hiddenWorksheetFactorySingleton = new HiddenWorksheetFactory();
