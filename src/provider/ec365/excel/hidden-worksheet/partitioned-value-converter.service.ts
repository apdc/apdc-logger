const CELL_MAX_CHARACTERS = 32760;

export class PartitionedValueConverter {

  partitionValue(value: any): string[] {
    const valueString = JSON.stringify(value);

    const partitionedValue = [];
    for (let i = 0; i < valueString.length; i += CELL_MAX_CHARACTERS) {
      partitionedValue.push(valueString.substr(i, CELL_MAX_CHARACTERS));
    }

    return partitionedValue;
  }

  convertFromPartitionedValue(partitionedValue: string[]): any {
    return JSON.parse(partitionedValue.join(''));
  }

}

export const partitionedValueConverterSingleton = new PartitionedValueConverter();
