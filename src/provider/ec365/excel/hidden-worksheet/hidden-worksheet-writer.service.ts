import { excelWorksheetApiSingleton } from '../api/excel-worksheet.api';

const FIRST_COLUMN = 0;

export class HiddenWorksheetWriter {

  async writeRow(hiddenWorksheetId: string, rowIndex: number, values: any[]): Promise<void> {
    await excelWorksheetApiSingleton.writeDataToWorksheetByWorksheetIdWithRiskyData(hiddenWorksheetId, rowIndex, FIRST_COLUMN, [values]);
  }

}

export const hiddenWorksheetWriterSingleton = new HiddenWorksheetWriter();
