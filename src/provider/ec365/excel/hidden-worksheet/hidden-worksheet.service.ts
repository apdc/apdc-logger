import { hiddenWorksheetFactorySingleton } from './hidden-worksheet-factory.service';
import { partitionedValueConverterSingleton } from './partitioned-value-converter.service';
import { hiddenWorksheetReaderSingleton } from './hidden-worksheet-reader.service';
import { hiddenWorksheetWriterSingleton } from './hidden-worksheet-writer.service';

export class HiddenWorksheetService {

  async setItem<T>(worksheetName: string, key: string, value: T) {
    const hiddenWorksheetId = await hiddenWorksheetFactorySingleton.getExistingOrCreate(worksheetName);
    const partitionedValue = partitionedValueConverterSingleton.partitionValue(value);
    let rowIndex = await this.indexOfRowWithKey(hiddenWorksheetId, key);

    if (rowIndex < 0) {
      rowIndex = await hiddenWorksheetReaderSingleton.getRowIndexForNewKey(hiddenWorksheetId);
    }

    const rowValues = [key, partitionedValue.length, ...partitionedValue];
    await hiddenWorksheetWriterSingleton.writeRow(hiddenWorksheetId, rowIndex, rowValues);
  }

  private async indexOfRowWithKey(hiddenWorksheetId: string, key: string): Promise<number> {
    const keys = await hiddenWorksheetReaderSingleton
      .getAllKeys(hiddenWorksheetId);

    return keys.findIndex(value => value === key);
  }

  public async getItem<T>(worksheetId: string, key: string): Promise<T> {
    const hiddenWorksheetId = await hiddenWorksheetFactorySingleton.getExistingOrCreate(worksheetId);

    const rowIndex = await this.indexOfRowWithKey(hiddenWorksheetId, key);
    if (rowIndex < 0) {
      return null;
    }

    try {
      return await this.readValueFromRow(hiddenWorksheetId, rowIndex);
    } catch (error) {
      console.error(`cannot parse value from hidden worksheet, key: <${key}>, worksheetId: <${worksheetId}>`, error);
      return null;
    }
  }

  private async readValueFromRow(hiddenWorksheetId: string, rowIndex: number): Promise<any> {
    const partitionedValue = await hiddenWorksheetReaderSingleton
      .readPartitionedValue(hiddenWorksheetId, rowIndex);
    return partitionedValueConverterSingleton.convertFromPartitionedValue(partitionedValue);
  }

}

export const hiddenWorksheetServiceSingleton = new HiddenWorksheetService();
