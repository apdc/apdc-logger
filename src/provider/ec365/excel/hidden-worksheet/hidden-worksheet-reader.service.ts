import { excelRangeApiSingleton } from '../api/excel-range.api';

const FIRST_VALUE_COLUMN_INDEX = 2;
const KEY_COLUMN_INDEX = 0;
const ONE_COLUMN = 1;
const ONE_ROW = 1;
const PARTS_COUNT_COLUMN_INDEX = 1;

export class HiddenWorksheetReader {

  async getAllKeys(hiddenWorksheetId: string): Promise<string[]> {
    const rangeValuesWithKeys = await this.getValuesWithAllKeys(hiddenWorksheetId);
    return rangeValuesWithKeys || [];
  }

  async getRowIndexForNewKey(hiddenWorksheetId: string): Promise<number> {
    return excelRangeApiSingleton.fetchUsedRangeRowCountByColumn(KEY_COLUMN_INDEX, hiddenWorksheetId);
  }

  async readPartitionedValue(hiddenWorksheetId: string, rowIndex: number): Promise<string[]> {
    const partsCount = await this.readsPartsCount(hiddenWorksheetId, rowIndex);
    const partsRangeValues = await excelRangeApiSingleton.fetchWorksheetRangeValuesByIndexes({
      worksheetId: hiddenWorksheetId,
      startRow: rowIndex,
      startColumn: FIRST_VALUE_COLUMN_INDEX,
      rowCount: ONE_ROW,
      columnCount: partsCount,
    });

    return partsRangeValues[0];
  }

  private async readsPartsCount(hiddenWorksheetId: string, rowIndex: number): Promise<number> {
    const valuesCellWithLength = await excelRangeApiSingleton.fetchWorksheetRangeValuesByIndexes({
      worksheetId: hiddenWorksheetId,
      startRow: rowIndex,
      startColumn: PARTS_COUNT_COLUMN_INDEX,
      rowCount: ONE_ROW,
      columnCount: ONE_COLUMN,
    });

    return parseInt(valuesCellWithLength[0][0], 10);
  }

  private getValuesWithAllKeys(hiddenWorksheetId: string): Promise<string[]> {
    return excelRangeApiSingleton.fetchUsedRangeValuesByColumn(hiddenWorksheetId, KEY_COLUMN_INDEX);
  }
}

export const hiddenWorksheetReaderSingleton = new HiddenWorksheetReader();
