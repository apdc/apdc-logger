import { FunctionLoaded } from '../loader-interface';

export const loadExcelApplication = (): FunctionLoaded<Excel.Application> => context => context.workbook.application;
