import { ExcelStreamBuilder } from '../builder/excel-stream.builder';
import { WORKSHEET_LOAD_PARAMS } from './worksheet/worksheet-load.params';
import { getWorksheetById, getWorksheetByName } from './worksheet/worksheets.actions';

interface ExcelWorksheetApiSettings {
  chunkCellsSize;
}

const DEFAULT_EXCEL_WORKSHEET_API_SETTINGS: ExcelWorksheetApiSettings = {
  chunkCellsSize: 500,
};

class ExcelWorksheetApi {
  private settings: ExcelWorksheetApiSettings = DEFAULT_EXCEL_WORKSHEET_API_SETTINGS; // should be refactored in the future per instance not global

  public changeSettings(settings: ExcelWorksheetApiSettings) {
    this.settings = {
      chunkCellsSize: settings.chunkCellsSize || this.settings.chunkCellsSize,
    };
  }

  public createNewWorksheet(worksheetName: string, worksheetVisibility: Excel.SheetVisibility): Promise<string> {
    return ExcelStreamBuilder
      .stream()
      .getWithSync((context): Excel.Worksheet => {
        const newWorksheet = context
          .workbook
          .worksheets
          .add(worksheetName)
          .load(WORKSHEET_LOAD_PARAMS.ID);

        newWorksheet.visibility = worksheetVisibility;
        return newWorksheet;
      })
      .map(worksheet => worksheet.id)
      .run();
  }

  public writeDataToWorksheetByWorksheetId(worksheetId: string, startRow: number, startColumn: number, values: any[][]): Promise<void> {
    return getWorksheetById(worksheetId)
      .actionWithSync(async (context, worksheet) => {
        worksheet
          .getRangeByIndexes(startRow, startColumn, values.length, values[0].length)
          .values = values;
      })
      .run();
  }

  public async writeDataToWorksheetByWorksheetIdWithRiskyData(worksheetId: string, startRow: number, startColumn: number, values: any[][]): Promise<void> {
    const tasks = [];
    const valuesToSave = this.convertAllRiskyData(values);
    const chunkCellsSize = this.settings.chunkCellsSize;

    for (let rowIndex = 0; rowIndex < valuesToSave.length; rowIndex += 1) {
      for (let colIndex = 0; colIndex < valuesToSave[rowIndex].length; colIndex += chunkCellsSize) {
        const chunkData = [valuesToSave[rowIndex].slice(colIndex, colIndex + chunkCellsSize)];
        tasks.push(this.writeDataToWorksheetByWorksheetId(worksheetId, startRow + rowIndex, startColumn + colIndex, chunkData));
      }
    }

    await Promise.all(tasks);
  }

  private convertAllRiskyData(values: any[][]): any[][] {
    const specialCharacterToAvoidFormulas = "'";
    for (let i = 0; i < values.length ; i = i + 1) {
      for (let j = 0; j < values[0].length ; j = j + 1) {
        const val = values[i][j].toString();

        values[i][j] = `${specialCharacterToAvoidFormulas}${val}`;
      }
    }

    return values;
  }

  public fetchWorksheetIdByName(worksheetName: string): Promise<string> {
    return getWorksheetByName(worksheetName)
      .getWithSync((context, worksheet): Excel.Worksheet => {
        if (!worksheet) {
          return undefined;
        }

        worksheet.load('id');
        return worksheet;
      })
      .map(worksheet => worksheet ? worksheet.id : undefined)
      .run();
  }

}

export const excelWorksheetApiSingleton = new ExcelWorksheetApi();
