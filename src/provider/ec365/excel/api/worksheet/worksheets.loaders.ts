import { FunctionLoaded } from '../loader-interface';

export const loadWorksheets = (): FunctionLoaded<Excel.WorksheetCollection> => {
  return (context): Excel.WorksheetCollection => {
    return context.workbook.worksheets;
  };
};

export const loadActiveWorksheet = (): FunctionLoaded<Excel.Worksheet> => {
  return (context) => {
    return context
      .workbook
      .worksheets
      .getActiveWorksheet();
  };
};

export const loadWorksheet = (worksheetId: string): FunctionLoaded<Excel.Worksheet> => {
  return (context) => {
    return context
      .workbook
      .worksheets
      .getItem(worksheetId);
  };
};
