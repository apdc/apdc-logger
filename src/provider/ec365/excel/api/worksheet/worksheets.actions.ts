import { ExcelStreamBuilder } from '../../builder/excel-stream.builder';
import { loadWorksheets } from './worksheets.loaders';
import { WORKSHEET_LOAD_PARAMS } from './worksheet-load.params';

export const loadAllWorksheets = (): ExcelStreamBuilder<Excel.Worksheet[]> => ExcelStreamBuilder
  .stream()
  .loadWithSync(loadWorksheets(), WORKSHEET_LOAD_PARAMS.ITEMS)
  .map(worksheets => worksheets.items);

export const getWorksheetById = (worksheetId: string): ExcelStreamBuilder<Excel.Worksheet> => ExcelStreamBuilder
  .stream()
  .get(context => context
      .workbook
      .worksheets
      .getItem(worksheetId),
  );

export const getWorksheetByName = (worksheetName: string): ExcelStreamBuilder<Excel.Worksheet> => loadAllWorksheets()
  .map(worksheets => worksheets.find(worksheet => worksheet.name === worksheetName));
