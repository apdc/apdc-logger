import { loadRangeByIndexes, LoadRangeParams } from './range/range.loaders';
import { getUsedRangeByColumn } from './range/range.actions';
import { ExcelStreamBuilder } from '../builder/excel-stream.builder';
import { RANGE_LOAD_PARAMS } from './range/range-load.params';

const identity = <T>(t: T) => t;
const EMPTY_ROW = [''];
const ONLY_ELEMENT_INDEX = 0;

export class ExcelRangeApi {

  public fetchWorksheetRangeValuesByIndexes(loadRangeParams: LoadRangeParams): Promise<string[][]> {
    return ExcelStreamBuilder
      .stream()
      .loadWithSync(loadRangeByIndexes(loadRangeParams), RANGE_LOAD_PARAMS.VALUES)
      .map(range => range.values)
      .run();
  }

  public fetchUsedRangeRowCountByColumn(columnIndex: number, worksheetId: string): Promise<number> {
    return getUsedRangeByColumn(columnIndex, worksheetId)
      .loadObjectWithSync(identity, [RANGE_LOAD_PARAMS.ROW_COUNT, RANGE_LOAD_PARAMS.ROW_INDEX])
      .map(range => range.rowCount + range.rowIndex)
      .run();
  }

  public fetchUsedRangeValuesByColumn(worksheetId: string, columnIndex: number): Promise<string[]> {
    return getUsedRangeByColumn(columnIndex, worksheetId)
      .loadObjectWithSync(identity, [RANGE_LOAD_PARAMS.VALUES, RANGE_LOAD_PARAMS.ROW_INDEX])
      .map((range: Excel.Range) => {
        const values = range.values || [];

        if (range.rowIndex === 0 || range.rowIndex === undefined) {
          return values;
        }

        return Array.from({ length: range.rowIndex }, () => EMPTY_ROW)
          .concat(values);
      })
      .map(values => values.map(row => row[ONLY_ELEMENT_INDEX]))
      .run();
  }

}

export const excelRangeApiSingleton = new ExcelRangeApi();
