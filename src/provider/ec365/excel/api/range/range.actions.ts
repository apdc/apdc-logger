import { LoadRangeParams } from './range.loaders';
import { ExcelStreamBuilder } from '../../builder/excel-stream.builder';

export const getRangeByIndexes = (loadRangeParams: LoadRangeParams): ExcelStreamBuilder<Excel.Range> => {
  const { worksheetId, startRow, startColumn, rowCount, columnCount } = loadRangeParams;

  return ExcelStreamBuilder
    .stream()
    .get(context => context
      .workbook
      .worksheets
      .getItem(worksheetId)
      .getRangeByIndexes(startRow, startColumn, rowCount, columnCount),
    );
};

export const getRangeByColumn = (columnIndex: number, worksheetId: string): ExcelStreamBuilder<Excel.Range> =>
  getRangeByIndexes({
    worksheetId,
    startRow: 0,
    startColumn: columnIndex,
    rowCount: 1,
    columnCount: 1,
  }).map((range: Excel.Range) => range.getEntireColumn());

export const getUsedRangeByColumn = (columnIndex: number, worksheetId: string): ExcelStreamBuilder<Excel.Range> =>
  getRangeByColumn(columnIndex, worksheetId).map((range: Excel.Range) => range.getUsedRangeOrNullObject());
