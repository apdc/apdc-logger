import { FunctionLoaded } from '../loader-interface';

export interface LoadRangeParams {

  readonly worksheetId: string;
  readonly startRow: number;
  readonly startColumn: number;
  readonly rowCount: number;
  readonly columnCount: number;

}

export const loadRangeByIndexes = (props: LoadRangeParams): FunctionLoaded<Excel.Range> => {
  const { startRow, startColumn, rowCount, columnCount } = props;

  return context => context
      .workbook
      .worksheets
      .getItem(props.worksheetId)
      .getRangeByIndexes(startRow, startColumn, rowCount, columnCount);
};
