export const RANGE_LOAD_PARAMS = {
  VALUES: 'values',
  COLUMN_COUNT: 'columnCount',
  COLUMN_INDEX: 'columnIndex',
  ROW_COUNT: 'rowCount',
  ROW_INDEX: 'rowIndex',
};
