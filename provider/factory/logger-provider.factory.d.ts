import { EcLoggerProvider, LoggerProvider, LoggerProviderSettings } from '../../core/type/loggerLevelTypes';
export declare class LoggerProviderFactory {
    static createConsoleLoggerProvider(): LoggerProvider;
    static createEc365LoggerProvider(worksheetName: string, loggerSettings?: LoggerProviderSettings): EcLoggerProvider;
}
