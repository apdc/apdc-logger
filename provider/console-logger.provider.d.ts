import { Log } from '../core/util/logger.utils';
import { AbstractLoggerProvider } from './abstract-logger.provider';
export declare class ConsoleLoggerProvider extends AbstractLoggerProvider {
    static create(): ConsoleLoggerProvider;
    private constructor();
    protected onDebug(log: Log): void;
    protected onError(log: Log): void;
    protected onInfo(log: Log): void;
    protected onWarn(log: Log): void;
}
