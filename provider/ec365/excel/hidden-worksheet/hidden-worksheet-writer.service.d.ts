export declare class HiddenWorksheetWriter {
    writeRow(hiddenWorksheetId: string, rowIndex: number, values: any[]): Promise<void>;
}
export declare const hiddenWorksheetWriterSingleton: HiddenWorksheetWriter;
