export declare class PartitionedValueConverter {
    partitionValue(value: any): string[];
    convertFromPartitionedValue(partitionedValue: string[]): any;
}
export declare const partitionedValueConverterSingleton: PartitionedValueConverter;
