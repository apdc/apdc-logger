export declare class HiddenWorksheetService {
    setItem<T>(worksheetName: string, key: string, value: T): Promise<void>;
    private indexOfRowWithKey;
    getItem<T>(worksheetId: string, key: string): Promise<T>;
    private readValueFromRow;
}
export declare const hiddenWorksheetServiceSingleton: HiddenWorksheetService;
