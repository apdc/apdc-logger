export declare class HiddenWorksheetFactory {
    getExistingOrCreate(worksheetId: string): Promise<string>;
    getHiddenWorksheetName(worksheetId: string): string;
    private createHiddenWorksheet;
}
export declare const hiddenWorksheetFactorySingleton: HiddenWorksheetFactory;
