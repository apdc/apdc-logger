export declare class HiddenWorksheetReader {
    getAllKeys(hiddenWorksheetId: string): Promise<string[]>;
    getRowIndexForNewKey(hiddenWorksheetId: string): Promise<number>;
    readPartitionedValue(hiddenWorksheetId: string, rowIndex: number): Promise<string[]>;
    private readsPartsCount;
    private getValuesWithAllKeys;
}
export declare const hiddenWorksheetReaderSingleton: HiddenWorksheetReader;
