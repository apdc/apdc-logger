/// <reference types="office-js" />
export interface LoaderType<T> {
    load(option?: string | string[]): T;
}
export interface FunctionLoaded<T extends LoaderType<T>> {
    (context: Excel.RequestContext): T;
}
