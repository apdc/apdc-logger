import { LoadRangeParams } from './range/range.loaders';
export declare class ExcelRangeApi {
    fetchWorksheetRangeValuesByIndexes(loadRangeParams: LoadRangeParams): Promise<string[][]>;
    fetchUsedRangeRowCountByColumn(columnIndex: number, worksheetId: string): Promise<number>;
    fetchUsedRangeValuesByColumn(worksheetId: string, columnIndex: number): Promise<string[]>;
}
export declare const excelRangeApiSingleton: ExcelRangeApi;
