/// <reference types="office-js" />
interface ExcelWorksheetApiSettings {
    chunkCellsSize: any;
}
declare class ExcelWorksheetApi {
    private settings;
    changeSettings(settings: ExcelWorksheetApiSettings): void;
    createNewWorksheet(worksheetName: string, worksheetVisibility: Excel.SheetVisibility): Promise<string>;
    writeDataToWorksheetByWorksheetId(worksheetId: string, startRow: number, startColumn: number, values: any[][]): Promise<void>;
    writeDataToWorksheetByWorksheetIdWithRiskyData(worksheetId: string, startRow: number, startColumn: number, values: any[][]): Promise<void>;
    private convertAllRiskyData;
    fetchWorksheetIdByName(worksheetName: string): Promise<string>;
}
export declare const excelWorksheetApiSingleton: ExcelWorksheetApi;
export {};
