/// <reference types="office-js" />
import { FunctionLoaded } from '../loader-interface';
export declare const loadWorksheets: () => FunctionLoaded<Excel.WorksheetCollection>;
export declare const loadActiveWorksheet: () => FunctionLoaded<Excel.Worksheet>;
export declare const loadWorksheet: (worksheetId: string) => FunctionLoaded<Excel.Worksheet>;
