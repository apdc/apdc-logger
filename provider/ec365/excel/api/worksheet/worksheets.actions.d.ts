/// <reference types="office-js" />
import { ExcelStreamBuilder } from '../../builder/excel-stream.builder';
export declare const loadAllWorksheets: () => ExcelStreamBuilder<Excel.Worksheet[]>;
export declare const getWorksheetById: (worksheetId: string) => ExcelStreamBuilder<Excel.Worksheet>;
export declare const getWorksheetByName: (worksheetName: string) => ExcelStreamBuilder<Excel.Worksheet>;
