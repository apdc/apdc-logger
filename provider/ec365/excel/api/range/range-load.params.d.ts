export declare const RANGE_LOAD_PARAMS: {
    VALUES: string;
    COLUMN_COUNT: string;
    COLUMN_INDEX: string;
    ROW_COUNT: string;
    ROW_INDEX: string;
};
