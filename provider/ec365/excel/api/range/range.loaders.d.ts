/// <reference types="office-js" />
import { FunctionLoaded } from '../loader-interface';
export interface LoadRangeParams {
    readonly worksheetId: string;
    readonly startRow: number;
    readonly startColumn: number;
    readonly rowCount: number;
    readonly columnCount: number;
}
export declare const loadRangeByIndexes: (props: LoadRangeParams) => FunctionLoaded<Excel.Range>;
