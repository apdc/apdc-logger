/// <reference types="office-js" />
import { LoadRangeParams } from './range.loaders';
import { ExcelStreamBuilder } from '../../builder/excel-stream.builder';
export declare const getRangeByIndexes: (loadRangeParams: LoadRangeParams) => ExcelStreamBuilder<Excel.Range>;
export declare const getRangeByColumn: (columnIndex: number, worksheetId: string) => ExcelStreamBuilder<Excel.Range>;
export declare const getUsedRangeByColumn: (columnIndex: number, worksheetId: string) => ExcelStreamBuilder<Excel.Range>;
