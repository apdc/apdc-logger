/// <reference types="office-js" />
import { FunctionLoaded } from '../loader-interface';
export declare const loadExcelApplication: () => FunctionLoaded<Excel.Application>;
