/// <reference types="office-js" />
import { FunctionLoaded, LoaderType } from '../api/loader-interface';
export declare class ExcelStreamBuilder<T> {
    private readonly collectedFunctions;
    private constructor();
    static stream(): ExcelStreamBuilder<void>;
    loadWithSync<S extends LoaderType<S>>(fun: FunctionLoaded<S>, propsToLoad?: string | string[]): ExcelStreamBuilder<S>;
    loadObjectWithSync<S extends LoaderType<S>>(fun: (prev: T) => S, propsToLoad?: string | string[]): ExcelStreamBuilder<S>;
    actionWithSync(fun: (context: Excel.RequestContext, prev?: T) => void): ExcelStreamBuilder<void>;
    action(fun: (context: Excel.RequestContext, prev?: T) => void | Promise<void>): ExcelStreamBuilder<void>;
    get<S>(fun: (context: Excel.RequestContext, prev?: T) => S): ExcelStreamBuilder<S>;
    getWithSync<S>(fun: (context: Excel.RequestContext, prev?: T) => S): ExcelStreamBuilder<S>;
    map<S>(fun: (prev: T) => S): ExcelStreamBuilder<S>;
    run(): Promise<T>;
}
