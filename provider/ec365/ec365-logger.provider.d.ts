import { Log } from '../../core/util/logger.utils';
import { EcLoggerProvider, LoggerProviderSettings } from '../../core/type/loggerLevelTypes';
export declare class Ec365LoggerProvider implements EcLoggerProvider {
    private worksheetName;
    private loggerSettings;
    private queueLogsPromise;
    private static ACTUAL_LOGS_KEY;
    private static PREVIOUSLY_LOGS_KEY;
    private logsBuffer;
    private areInitLogsFetched;
    private previouslyLogs;
    private bufferLogs;
    private debounceSave;
    static create(worksheetName: string, loggerSettings?: LoggerProviderSettings): Ec365LoggerProvider;
    private constructor();
    onLog(log: Log): void;
    changeSettings(loggerSettings: LoggerProviderSettings): void;
    private validateSettings;
    private nextLog;
    private saveLogsToWorksheet;
    private saveLogsFromBuffer;
    private createLogsObjectBasedOn;
    private removeLogsOlderThat;
    private generateNextIndex;
    private initPreviouslyLogs;
    private fetchActualLogs;
}
