import { LoggerProvider, LoggerLevelTypes } from '../core/type/loggerLevelTypes';
import { Log } from '../core/util/logger.utils';
export declare abstract class AbstractLoggerProvider implements LoggerProvider {
    protected decisionMap: {
        INFO: (log: any) => void;
        DEBUG: (log: any) => void;
        ERROR: (log: any) => void;
        WARN: (log: any) => void;
    };
    onLog(log: Log): void;
    protected abstract onInfo(log: Log): void;
    protected abstract onDebug(log: Log): void;
    protected abstract onWarn(log: Log): void;
    protected abstract onError(log: Log): void;
}
