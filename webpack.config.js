const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const TSLintPlugin = require('tslint-webpack-plugin');

const coreModulePath = {
    source: './src/core',
    dest: './core'
};

const decoratorModulePath = {
    source: './src/decorator',
    dest: './decorator'
};

const providerModulePath = {
    source: './src/provider',
    dest: './provider'
};

module.exports = {
    mode: 'production',
    entry: {
        'core/index': './src/core/index.ts',
        'decorator/index': './src/decorator/index.ts',
        'provider/index': './src/provider/index.ts',
    },
    module: {
        rules: [
            {
                test: /\.ts?$/,
                exclude: /node_modules/,
                loader: 'ts-loader',
            },
        ],
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js', '.json' ],
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, './'),
        library: 'apdc-logger',
        libraryTarget: 'umd',
        umdNamedDefine: true,
    },
    plugins: [
        new TSLintPlugin({
            files: [
                './src/**/*.ts'
            ],
            warningsAsError: true,
        }),
        new CopyPlugin([
            { from: `${coreModulePath.source}/package.json`, to: `${coreModulePath.dest}/package.json` },
            { from: `${decoratorModulePath.source}/package.json`, to: `${decoratorModulePath.dest}/package.json` },
            { from: `${providerModulePath.source}/package.json`, to: `${providerModulePath.dest}/package.json` },
        ]),
    ]
};
