import { LoggerLevelTypes } from '../type/loggerLevelTypes';
export interface Log {
    readonly timestamp: number;
    readonly type: LoggerLevelTypes;
    readonly data: string;
}
export declare class LoggerUtils {
    static createLog(data: string, type: LoggerLevelTypes): Log;
    static defaultLogToString(log: Log): string;
    private static timestampToUTCString;
}
