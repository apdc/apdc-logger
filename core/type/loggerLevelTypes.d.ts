import { Log } from '../util/logger.utils';
export declare enum LoggerLevelTypes {
    INFO = "INFO",
    DEBUG = "DEBUG",
    WARN = "WARN",
    ERROR = "ERROR"
}
export interface LoggerProviderSettings {
    removeOlderThat?: number;
    debounceTime?: number;
    chunkCellSize?: number;
}
export declare type LoggerLevelLiteralsTypes = 'INFO' | 'DEBUG' | 'WARN' | 'ERROR';
export interface LoggerProvider {
    onLog(log: Log): void;
}
export interface EcLoggerProvider extends LoggerProvider {
    changeSettings(loggerSettings: LoggerProviderSettings): void;
}
export interface Config {
    configLevelTypes: LoggerLevelTypes[] | LoggerLevelLiteralsTypes[];
}
