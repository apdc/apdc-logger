import { Config, LoggerProvider } from '../type/loggerLevelTypes';
export declare class LoggerService {
    private loggerWriter;
    private config;
    constructor(loggerWriter: LoggerProvider, config?: Config);
    changeConfig(config: Config): void;
    private log;
    logInfo(data: string): void;
    logWarn(data: string): void;
    logDebug(data: string): void;
    logError(data: string): void;
}
