import { Config, LoggerProvider } from '../type/loggerLevelTypes';
export declare class LoggerStaticService {
    private static loggerProvider;
    private static config;
    static register(loggerProvider: LoggerProvider, config?: Config): void;
    static getMyself(): typeof LoggerStaticService;
    static changeConfig(config: Config): void;
    private static log;
    private static checkIfShouldBeLogged;
    static logInfo(data: string): void;
    static logWarn(data: string): void;
    static logDebug(data: string): void;
    static logError(data: string): void;
}
