import { Config, LoggerProvider } from '../core/type/loggerLevelTypes';
export declare function registerLogger(loggerProvider: LoggerProvider, config?: Config): void;
export declare function changeLoggerConfig(config: Config): void;
export declare function debugClass(): ClassDecorator;
